import { combineReducers } from "redux";
import { numberReducer } from "./numberReducer";

export const rootReducer_ExReduxMini = combineReducers({
  numberReducer: numberReducer,
});
