import React, { Component } from "react";
import { connect } from "react-redux";

class ExReduxMini extends Component {
  render() {
    return (
      <div>
        <button
          onClick={this.props.handleGiamSoLuong}
          className="btn btn-danger"
        >
          -
        </button>
        <strong className="mx-3">{this.props.soLuong}</strong>
        <button
          onClick={this.props.handleTangSoLuong}
          className="btn btn-success"
        >
          +
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    soLuong: state.numberReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleTangSoLuong: () => {
      let action = {
        type: "TANG_SO_LUONG",
      };
      dispatch(action);
    },
    handleGiamSoLuong: () => {
      let action = {
        type: "GIAM_SO_LUONG",
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExReduxMini);
