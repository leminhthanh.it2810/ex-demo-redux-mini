import logo from './logo.svg';
import './App.css';
import ExReduxMini from './Ex_redux_mini/ExReduxMini';

function App() {
  return (
    <div className="App">
      <ExReduxMini />
    </div>
  );
}

export default App;
